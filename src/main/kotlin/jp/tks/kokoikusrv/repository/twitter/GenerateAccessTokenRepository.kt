package jp.tks.kokoikusrv.repository.twitter

import jp.tks.kokoikusrv.infrastructure.TwitterAccessToken

interface GenerateAccessTokenRepository {
    suspend fun generateToken(): TwitterAccessToken
}