package jp.tks.kokoikusrv.repository.twitter

import jp.tks.kokoikusrv.infrastructure.ApiRequestComponent
import jp.tks.kokoikusrv.infrastructure.TwitterAccessToken
import jp.tks.kokoikusrv.infrastructure.TwitterApiRequestInterface
import jp.tks.kokoikusrv.repository.cache.CacheRepositoryImpl
import kotlinx.coroutines.*
import okhttp3.ResponseBody
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.*


@DelicateCoroutinesApi
@Component
class GenerateAccessTokenRepositoryImpl : GenerateAccessTokenRepository {

    @Autowired
    lateinit var cacheRepository: CacheRepositoryImpl

    @Autowired
    lateinit var apiRequestComponent: ApiRequestComponent

    val RESOURCE_NAME = "kokoikuconfig"

    override suspend fun generateToken(): TwitterAccessToken =
        suspendCancellableCoroutine<TwitterAccessToken> { continuation ->
            val resourceBundle = ResourceBundle.getBundle(RESOURCE_NAME)
            val apiComponent =
                apiRequestComponent.createBuilderForTwitter().create(TwitterApiRequestInterface::class.java)
            val oauthToken = cacheRepository.getTwitterRequestTokenFromCache()
                ?: throw TwitterGetAccessTokenFailedBuildParameterException()
            val verifier = cacheRepository.getTwitterVerifierFromCache()
                ?: throw TwitterGetAccessTokenFailedBuildParameterException()
            GlobalScope.launch(Dispatchers.IO) {
                apiComponent.getAccessToken(
                    oauthToken = oauthToken.token,
                    oauthConsumerKey = resourceBundle.getString("api_key"),
                    oauthVerifier = verifier
                ).enqueue(object : Callback<ResponseBody> {
                    override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                        // AccessTokenの取得成功時はキャッシュに保存する
                        val responseString = response.body()?.string()!!
                        val twitterAccessToken = TwitterAccessToken(
                            responseString.split("&")[0].replace(
                                "oauth_token", ""
                            ), responseString.split("&")[1].replace("oauth_token_secret", "")
                        )
                        cacheRepository.setAccessTokenToCache(twitterAccessToken)
                        continuation.resume(twitterAccessToken, onCancellation = {
                            it.printStackTrace()
                            throw TwitterGetAccessTokenFailedCannotGet(it)
                        })
                    }

                    override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                        t.printStackTrace()
                        throw TwitterGetAccessTokenFailedCannotGet(t)
                    }
                })
            }
        }
}

class TwitterGetAccessTokenFailedBuildParameterException : Exception()
class TwitterGetAccessTokenFailedCannotGet(e: Throwable) : Exception()