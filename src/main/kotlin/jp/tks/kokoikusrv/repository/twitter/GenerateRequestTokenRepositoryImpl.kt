package jp.tks.kokoikusrv.repository.twitter

import jp.tks.kokoikusrv.Twitter4jConfig
import kotlinx.coroutines.suspendCancellableCoroutine
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component
import twitter4j.auth.OAuth2Token
import twitter4j.auth.RequestToken

@Component
class GenerateRequestTokenRepositoryImpl : GenerateRequestTokenRepository {

    @Autowired
    lateinit var twitter4jConfig: Twitter4jConfig


    override fun generateToken(): Result<RequestToken> {
        // リクエストトークンを取得
        return runCatching {
            twitter4jConfig.twitter.oAuthRequestToken
        }.onSuccess {
            Result.success(it)
        }.onFailure {
            Result.failure<Throwable>(it)
        }
    }
}