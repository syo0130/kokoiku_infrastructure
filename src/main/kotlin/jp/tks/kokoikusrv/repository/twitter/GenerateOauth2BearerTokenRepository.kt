package jp.tks.kokoikusrv.repository.twitter

import jp.tks.kokoikusrv.infrastructure.TwitterOauth2Token

interface GenerateOauth2BearerTokenRepository {
    suspend fun generateBearerToken(): TwitterOauth2Token
}