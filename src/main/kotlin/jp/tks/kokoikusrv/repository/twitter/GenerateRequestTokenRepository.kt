package jp.tks.kokoikusrv.repository.twitter

import twitter4j.auth.RequestToken

interface GenerateRequestTokenRepository {
    fun generateToken(): Result<RequestToken>
}