package jp.tks.kokoikusrv.repository.twitter

import com.google.gson.Gson
import jp.tks.kokoikusrv.infrastructure.ApiRequestComponent
import jp.tks.kokoikusrv.infrastructure.TwitterApiRequestInterface
import jp.tks.kokoikusrv.infrastructure.TwitterOauth2Token
import jp.tks.kokoikusrv.repository.cache.CacheRepositoryImpl
import kotlinx.coroutines.*
import okhttp3.ResponseBody
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

@DelicateCoroutinesApi
@Component
class GenerateOauth2BearerTokenRepositoryImpl : GenerateOauth2BearerTokenRepository {
    @Autowired
    lateinit var cacheRepository: CacheRepositoryImpl

    @Autowired
    lateinit var apiRequestComponent: ApiRequestComponent

    override suspend fun generateBearerToken(): TwitterOauth2Token =
        suspendCancellableCoroutine<TwitterOauth2Token> { continuation ->
            val apiComponent = apiRequestComponent.createBuilderForTwitterAuthorizationBearerToken()
                .create(TwitterApiRequestInterface::class.java)
            GlobalScope.launch(Dispatchers.IO) {
                apiComponent.generateOauth2Token().enqueue(object : Callback<ResponseBody> {
                    override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                        // Bearer Tokenの取得成功時はキャッシュに保存する
                        val responseString = response.body()?.string()!!
                        val bearerToken = Gson().fromJson(Gson().toJson(responseString), TwitterOauth2Token::class.java)
                        cacheRepository.setOauth2AccessTokenToCache(bearerToken)
                        continuation.resume(bearerToken, onCancellation = {
                            it.printStackTrace()
                            throw TwitterGenerateOauth2TokenFailedBuildParameterException(it)
                        })
                    }

                    override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                        t.printStackTrace()
                        throw TwitterGetAccessTokenFailedCannotGet(t)
                    }
                })
            }
        }
}


class TwitterGenerateOauth2TokenFailedBuildParameterException(t: Throwable) : Exception()