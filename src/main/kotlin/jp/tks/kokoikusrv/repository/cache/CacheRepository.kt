package jp.tks.kokoikusrv.repository.cache

import jp.tks.kokoikusrv.infrastructure.TwitterAccessToken
import jp.tks.kokoikusrv.infrastructure.TwitterOauth2Token
import twitter4j.auth.RequestToken

interface CacheRepository {
    fun getTwitterRequestTokenFromCache(): RequestToken?
    fun setTwitterRequestTokenToCache(twitterRequestToken: RequestToken): RequestToken?

    fun setTwitterVerifierToCache(verifier: String?): String?
    fun getTwitterVerifierFromCache(): String?

    fun setAccessTokenToCache(twitterAccessToken: TwitterAccessToken?): TwitterAccessToken?
    fun getAccessTokenFromCache(): TwitterAccessToken?

    fun setOauth2AccessTokenToCache(twitterOauth2Token: TwitterOauth2Token): TwitterOauth2Token?
    fun getOauth2AccessTokenFromCache(): TwitterOauth2Token?
}