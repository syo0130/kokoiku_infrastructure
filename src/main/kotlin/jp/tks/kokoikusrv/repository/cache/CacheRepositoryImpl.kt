package jp.tks.kokoikusrv.repository.cache

import jp.tks.kokoikusrv.CacheKokoikuConfig
import jp.tks.kokoikusrv.infrastructure.TwitterAccessToken
import jp.tks.kokoikusrv.infrastructure.TwitterOauth2Token
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component
import twitter4j.auth.RequestToken

@Component
class CacheRepositoryImpl : CacheRepository {

    @Autowired
    lateinit var cacheKokoikuConfig: CacheKokoikuConfig

    override fun getTwitterRequestTokenFromCache(): RequestToken? {
        cacheKokoikuConfig.cacheManager()?.let {
            return it.getCache("twitterRequestToken")?.get("token", RequestToken::class.java)
        }
        return null
    }

    override fun setTwitterRequestTokenToCache(twitterRequestToken: RequestToken): RequestToken? {
        setTwitterRequestTokenToCachePrivate(twitterRequestToken)
        return twitterRequestToken
    }

    override fun setTwitterVerifierToCache(verifier: String?): String? {
        verifier?.let {
            cacheKokoikuConfig.cacheManager()?.getCache("twitterVerifier")?.put("verifier", it)
        }
        return verifier
    }

    override fun getTwitterVerifierFromCache(): String? {
        return getValueFromCaChe("twitterVerifier", "verifier")

    }

    override fun setAccessTokenToCache(twitterAccessToken: TwitterAccessToken?): TwitterAccessToken? {
        twitterAccessToken?.let {
            cacheKokoikuConfig.cacheManager()?.getCache("twitterAccessToken")?.put("token", it)
        }
        return null
    }

    override fun getAccessTokenFromCache(): TwitterAccessToken? {
        return cacheKokoikuConfig.cacheManager()?.let {
            return it.getCache("twitterAccessToken")?.get("token", TwitterAccessToken::class.java)
        }
    }

    override fun setOauth2AccessTokenToCache(twitterOauth2Token: TwitterOauth2Token): TwitterOauth2Token? {
        twitterOauth2Token.let {
            cacheKokoikuConfig.cacheManager()?.getCache("twitterOauth2AccessToken")?.put("token", it)
        }
        return null
    }

    override fun getOauth2AccessTokenFromCache(): TwitterOauth2Token? {
        return cacheKokoikuConfig.cacheManager()?.let {
            return it.getCache("twitterOauth2AccessToken")?.get("token", TwitterOauth2Token::class.java)
        }
    }

    private fun getValueFromCaChe(name: String, key: String): String? {
        cacheKokoikuConfig.cacheManager()?.let {
            return it.getCache(name)?.get(key, String::class.java)
        }
        return null
    }


    fun setTwitterRequestTokenToCachePrivate(twitterRequestToken: RequestToken) {
        cacheKokoikuConfig.cacheManager()?.getCache("twitterRequestToken")?.put("token", twitterRequestToken)
    }
}