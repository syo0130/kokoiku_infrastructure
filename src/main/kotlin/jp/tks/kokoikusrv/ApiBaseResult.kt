package jp.tks.kokoikusrv

sealed class ApiBaseResult<out R> {
    data class Success<out T>(val data: T) : ApiBaseResult<T>()
    data class Error(val exception: Throwable) : ApiBaseResult<Nothing>()
}