package jp.tks.kokoikusrv

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication
import org.springframework.cache.annotation.EnableCaching

@EnableCaching
@SpringBootApplication(scanBasePackages = ["jp.tks.kokoikusrv"])
class KokoikusrvApplication


fun main(args: Array<String>) {
    runApplication<KokoikusrvApplication>(*args)
}
