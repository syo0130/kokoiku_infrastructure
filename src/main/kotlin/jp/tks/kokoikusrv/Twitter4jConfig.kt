package jp.tks.kokoikusrv

import jp.tks.kokoikusrv.infrastructure.TwitterAccessToken
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import twitter4j.HttpRequest
import twitter4j.Twitter
import twitter4j.TwitterFactory
import twitter4j.auth.Authorization
import twitter4j.conf.ConfigurationBuilder
import java.util.*


@Configuration
class Twitter4jConfig {

    lateinit var twitter: Twitter

    val RESOURCE_NAME = "kokoikuconfig"

    @Bean
    fun getTwitterInstanceBeforeLogin() {
        // twitterインスタンスにConsumer keyとConsumer Secretを設定
        val resourceBundle = ResourceBundle.getBundle(RESOURCE_NAME)
        val configurationBuilder = ConfigurationBuilder()
        configurationBuilder.setDebugEnabled(true).setOAuthConsumerKey(resourceBundle.getString("api_key"))
            .setOAuthConsumerSecret(resourceBundle.getString("api_secret")).setOAuthAccessToken(null)
            .setOAuthAccessTokenSecret(null)
        twitter = TwitterFactory(configurationBuilder.build()).instance
    }

    suspend fun getTwitterInstance(twitterAccessToken: TwitterAccessToken, authorizationBearer: String): Twitter {
        // twitterインスタンスにConsumer keyとConsumer Secretを設定し、更にAccessTokenも設定する。
        val resourceBundle = ResourceBundle.getBundle(RESOURCE_NAME)
        val configurationBuilder = ConfigurationBuilder()
        configurationBuilder.setDebugEnabled(true).setOAuthConsumerKey(resourceBundle.getString("api_key"))
            .setOAuthConsumerSecret(resourceBundle.getString("api_secret"))
            .setOAuthAccessToken(twitterAccessToken.token).setOAuthAccessTokenSecret(twitterAccessToken.secretToken)
        twitter = TwitterFactory(configurationBuilder.build()).getInstance(BearerAuthorization(authorizationBearer))
        return twitter
    }

    /**
     * Authorization Bearer設定用クラス
     */
    class BearerAuthorization internal constructor(bearer: String?) : Authorization {
        private val bearer: String?

        init {
            this.bearer = bearer
        }

        override fun getAuthorizationHeader(req: HttpRequest?): String {
            return "Bearer $bearer"
        }

        override fun isEnabled(): Boolean {
            return bearer != null
        }
    }


}