package jp.tks.kokoikusrv.usecase

import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.channelFlow
import kotlinx.coroutines.runBlocking
import org.springframework.stereotype.Component

@Component
class UseCaseLogout {

    /**
     * ログアウト時は下記の挙動を行う。
     * ・UserSessionをクリアする
     * ・ログイン画面に遷移する
     */
    @OptIn(ExperimentalCoroutinesApi::class)
    fun executeTwitterLogout(): Flow<Result<Boolean>> = channelFlow {
        runBlocking {
            channel.trySend(Result.success(true))
        }
    }
}