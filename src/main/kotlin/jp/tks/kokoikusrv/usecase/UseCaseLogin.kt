package jp.tks.kokoikusrv.usecase

import jp.tks.kokoikusrv.repository.cache.CacheRepositoryImpl
import jp.tks.kokoikusrv.repository.twitter.GenerateRequestTokenRepositoryImpl
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component

@Component
class UseCaseLogin {

    @Autowired
    lateinit var getRequestTokenRepository: GenerateRequestTokenRepositoryImpl


    @Autowired
    lateinit var cacheRepository: CacheRepositoryImpl


    fun executeTwitterLogin(): String? {
        runCatching {
            val requestToken = getRequestTokenRepository.generateToken().getOrNull()
            val twitterOauthRedirectUrl = requestToken?.authenticationURL
            // 認証URLが取得できた場合キャッシュにリクエストトークンとverifierを保管する
            cacheRepository.setTwitterRequestTokenToCache(
                twitterRequestToken = requestToken!!
            )
            return twitterOauthRedirectUrl
        }.onFailure {
            it.printStackTrace()
        }
        return null
    }
}

class LoginFailedNotTwitterRequestTokenException : Exception()