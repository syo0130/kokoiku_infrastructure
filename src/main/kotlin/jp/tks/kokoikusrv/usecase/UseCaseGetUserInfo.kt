@file:OptIn(DelicateCoroutinesApi::class)

package jp.tks.kokoikusrv.usecase

import jp.tks.kokoikusrv.Twitter4jConfig
import jp.tks.kokoikusrv.infrastructure.TwitterAccessToken
import jp.tks.kokoikusrv.repository.cache.CacheRepositoryImpl
import jp.tks.kokoikusrv.repository.twitter.GenerateAccessTokenRepositoryImpl
import jp.tks.kokoikusrv.repository.twitter.GenerateOauth2BearerTokenRepositoryImpl
import jp.tks.kokoikusrv.repository.twitter.TwitterGenerateOauth2TokenFailedBuildParameterException
import kotlinx.coroutines.DelicateCoroutinesApi
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component
import twitter4j.User

@Component
class UseCaseGetUserInfo {

    @Autowired
    lateinit var twitter4jConfig: Twitter4jConfig

    @Autowired
    lateinit var generateAccessTokenRepository: GenerateAccessTokenRepositoryImpl

    @Autowired
    lateinit var generateOauth2BearerTokenRepository: GenerateOauth2BearerTokenRepositoryImpl

    @Autowired
    lateinit var cacheRepository: CacheRepositoryImpl

    suspend fun getUserInfo(): Result<User> = runCatching {
        val accessToken = generateAccessTokenRepository.generateToken()
        val authorizationBearer = cacheRepository.getOauth2AccessTokenFromCache()
        val twitter = twitter4jConfig.getTwitterInstance(
            twitterAccessToken = TwitterAccessToken(
                accessToken.token,
                accessToken.secretToken
            ), authorizationBearer = authorizationBearer?.bearerToken ?: throw GetTwitterInfoFailedException()
        )
        twitter.verifyCredentials()
    }.onSuccess {
        Result.success(it)
    }.onFailure {
        // Ouath2API実行に失敗した場合はBearerTokenを再発行する
        generateOauth2BearerTokenRepository.generateBearerToken()
        throw TwitterGenerateOauth2TokenFailedBuildParameterException(it)
    }
}

class GetTwitterInfoFailedException : Exception()