@file:OptIn(DelicateCoroutinesApi::class)

package jp.tks.kokoikusrv.controller

import jp.tks.kokoikusrv.repository.cache.CacheRepositoryImpl
import jp.tks.kokoikusrv.repository.twitter.GenerateAccessTokenRepositoryImpl
import jp.tks.kokoikusrv.repository.twitter.GenerateRequestTokenRepositoryImpl
import jp.tks.kokoikusrv.repository.twitter.TwitterGenerateOauth2TokenFailedBuildParameterException
import jp.tks.kokoikusrv.usecase.*
import kotlinx.coroutines.DelicateCoroutinesApi
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.async
import kotlinx.coroutines.runBlocking
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Controller
import org.springframework.ui.Model
import org.springframework.ui.set
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestParam

@Controller
class LoginController {

    @Autowired
    lateinit var useCaseLogin: UseCaseLogin

    @Autowired
    lateinit var useCaseLogout: UseCaseLogout

    @Autowired
    lateinit var useCaseGetUserInfo: UseCaseGetUserInfo

    @Autowired
    lateinit var cacheRepository: CacheRepositoryImpl

    @Autowired
    lateinit var generateAccessTokenRepository: GenerateAccessTokenRepositoryImpl

    @Autowired
    lateinit var generateRequestTokenRepository: GenerateRequestTokenRepositoryImpl

    @RequestMapping("/s")
    fun loginpage(model: Model): String {
        return "twitterlogin"
    }

    @RequestMapping("/loginexec")
    suspend fun login(model: Model): String {
        runCatching {
            useCaseLogin.executeTwitterLogin()?.let {
                return "redirect:$it"
            }
        }.onFailure {
            when (it) {
                is LoginFailedNotTwitterRequestTokenException -> {
                    useCaseLogout.executeTwitterLogout()
                    return "twitterlogin"
                }
                else -> {
                    it.printStackTrace()
                }
            }
        }
        return "twitterlogin"
    }

    @RequestMapping("/content/")
    fun content(@RequestParam("oauth_verifier") verifier: String, model: Model): String {
        cacheRepository.setTwitterVerifierToCache(verifier)
        runBlocking {
            try {
                val getUserJob = GlobalScope.async { useCaseGetUserInfo.getUserInfo() }
                val user = getUserJob.await()
                model["user"] = user
                return@runBlocking "content"
            } catch (e: Exception) {
                when (e) {
                    is TwitterGenerateOauth2TokenFailedBuildParameterException, is GetTwitterInfoFailedException -> {
                        // Ouath2API実行に失敗した場合はBearer Tokenを発行し再リクエストする
                        val getUserJob = GlobalScope.async { useCaseGetUserInfo.getUserInfo() }
                        val user = getUserJob.await()
                        model["user"] = user
                        return@runBlocking "content"
                    }
                    else -> {
                        e.printStackTrace()
                    }
                }
            }
        }
        return "content"
    }
}