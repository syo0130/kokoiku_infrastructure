package jp.tks.kokoikusrv

import com.github.benmanes.caffeine.cache.Caffeine
import com.github.benmanes.caffeine.cache.CaffeineSpec
import com.github.benmanes.caffeine.cache.RemovalCause
import com.github.benmanes.caffeine.cache.RemovalListener
import org.springframework.cache.CacheManager
import org.springframework.cache.caffeine.CaffeineCacheManager
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import java.util.concurrent.TimeUnit

@Configuration
class CacheKokoikuConfig {
    /**
     * initialCapacity
     * maximumSize
     * maximumWeight
     * expireAfterAccess
     * expireAfterWrite
     * refreshAfterWrite
     * weakKeys
     * weakValues
     * softValues
     * recordStats
     */
    @Bean
    fun cacheManager(): CacheManager? {
        val specAsString = "initialCapacity=100,maximumSize=500,expireAfterAccess=5m,recordStats"
        val cacheManager = CaffeineCacheManager(
            "twitterRequestToken",
            "twitterRequestSecretToken",
            "twitterVerifier",
            "twitterAccessToken"
        )
        cacheManager.isAllowNullValues = false //can happen if you get a value from a @Cachable that returns null
        //cacheManager.setCacheSpecification(specAsString);
        //cacheManager.setCaffeineSpec(caffeineSpec());
        cacheManager.setCaffeine(caffeineCacheBuilder())
        return cacheManager
    }

    fun caffeineSpec(): CaffeineSpec? {
        return CaffeineSpec.parse("initialCapacity=100,maximumSize=500,expireAfterAccess=5m,recordStats")
    }

    fun caffeineCacheBuilder(): Caffeine<Any?, Any?> {
        return Caffeine.newBuilder().initialCapacity(100).maximumSize(150).expireAfterAccess(5, TimeUnit.MINUTES)
            .weakKeys().removalListener(CustomRemovalListener()).recordStats()
    }

    internal class CustomRemovalListener : RemovalListener<Any?, Any?> {
        override fun onRemoval(key: Any?, value: Any?, cause: RemovalCause) {
            System.out.format(
                "removal listerner called with key [%s], cause [%s], evicted [%S]\n",
                key,
                cause.toString(),
                cause.wasEvicted()
            )
        }
    }
}