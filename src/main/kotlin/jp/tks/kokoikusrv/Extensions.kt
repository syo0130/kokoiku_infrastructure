package jp.tks.kokoikusrv


fun String.getUrlQueryMap(): Map<String, String> {
    val params = substring(this.indexOf("?") + 1).trim().split("&").toTypedArray()
    val map: MutableMap<String, String> = HashMap()
    for (param in params) {
        val name = param.split("=").toTypedArray()[0]
        val value = param.split("=").toTypedArray()[1]
        map[name] = value
    }
    return map
}