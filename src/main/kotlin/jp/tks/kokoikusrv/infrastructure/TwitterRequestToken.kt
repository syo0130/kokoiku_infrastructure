package jp.tks.kokoikusrv.infrastructure

import com.google.gson.annotations.SerializedName
import org.springframework.stereotype.Component
import java.io.Serializable

@Component
data class TwitterRequestToken(
    val token: String? = null,
    val secretToken: String? = null
): Serializable