package jp.tks.kokoikusrv.infrastructure

import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.http.Field
import retrofit2.http.FormUrlEncoded
import retrofit2.http.POST


interface TwitterApiRequestInterface {

    @POST("/oauth/access_token")
    @FormUrlEncoded
    fun getAccessToken(
        @Field("oauth_consumer_key") oauthConsumerKey: String,

        @Field("oauth_token") oauthToken: String,

        @Field("oauth_verifier") oauthVerifier: String,
    ): Call<ResponseBody>

    @POST("/oauth2/token")
    fun generateOauth2Token(): Call<ResponseBody>
}