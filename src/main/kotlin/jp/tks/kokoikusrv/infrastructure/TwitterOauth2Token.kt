package jp.tks.kokoikusrv.infrastructure

import com.google.gson.annotations.SerializedName

data class TwitterOauth2Token(

    @SerializedName("token_type") val tokenType: String? = null,

    @SerializedName("access_token") val bearerToken: String? = null

) : java.io.Serializable