package jp.tks.kokoikusrv.infrastructure

import com.google.gson.GsonBuilder
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.Request
import okhttp3.Response
import okhttp3.logging.HttpLoggingInterceptor
import org.springframework.context.annotation.Bean
import org.springframework.stereotype.Component
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.nio.charset.Charset
import java.nio.charset.StandardCharsets
import java.util.*


@Component
class ApiRequestComponent {

    @Bean
    fun createBuilder(): Retrofit {
        val httpClient = OkHttpClient.Builder()
        return Retrofit.Builder().baseUrl("https://api.github.com/").addConverterFactory(GsonConverterFactory.create())
            .client(httpClient.build()).build()
    }


    fun createBuilderForTwitter(): Retrofit {
        val logging = HttpLoggingInterceptor()
        val gson = GsonBuilder()
            .setLenient()
            .create()
        logging.level = HttpLoggingInterceptor.Level.BASIC
        val httpClient = OkHttpClient.Builder().addInterceptor(logging)
        return Retrofit.Builder().baseUrl("https://api.twitter.com/").addConverterFactory(GsonConverterFactory.create(gson))
            .client(httpClient.build()).build()
    }

    fun createBuilderForTwitterAuthorizationBearerToken(): Retrofit {
        val logging = HttpLoggingInterceptor()
        val gson = GsonBuilder()
            .setLenient()
            .create()
        logging.level = HttpLoggingInterceptor.Level.BASIC
        val httpClient = OkHttpClient.Builder().addInterceptor(logging).addInterceptor(AuthorizationInterceptor())
        return Retrofit.Builder().baseUrl("https://api.twitter.com/").addConverterFactory(GsonConverterFactory.create(gson))
            .client(httpClient.build()).build()
    }
}



/**
 * 認証鍵生成クラス
 */
class AuthorizationInterceptor : Interceptor {
    /**
     * retrofitクライアントにAuthorization Basicに割り込みさせるためのメソッド
     */
    override fun intercept(chain: Interceptor.Chain): Response {
        val newRequest = chain.request().signedRequest()
        return chain.proceed(newRequest)
    }

    private fun Request.signedRequest(): Request {
        val RESOURCE_NAME = "kokoikuconfig"
        val charset: Charset = StandardCharsets.UTF_8
        val resourceBundle = ResourceBundle.getBundle(RESOURCE_NAME)
        val bearerTokenCredentials = Base64.getEncoder().encodeToString(
            resourceBundle.getString("api_key").toByteArray(charset)
        ) + ":" + Base64.getEncoder().encodeToString(
            resourceBundle.getString("api_secret").toByteArray(charset)
        )
        return newBuilder()
            .header("Authorization", "Basic $bearerTokenCredentials")
            .build()
    }
}