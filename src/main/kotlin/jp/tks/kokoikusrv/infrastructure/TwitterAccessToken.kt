package jp.tks.kokoikusrv.infrastructure

import com.google.gson.annotations.SerializedName
import java.io.Serializable

data class TwitterAccessToken(
    @JvmField @SerializedName("oauth_token") var token: String? = null,

    @JvmField @SerializedName("oauth_token_secret") var secretToken: String? = null,

    @JvmField @SerializedName("screen_name") var screenName: String? = null
) : Serializable
