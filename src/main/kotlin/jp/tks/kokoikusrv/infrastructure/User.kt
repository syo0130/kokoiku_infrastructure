package jp.tks.kokoikusrv.infrastructure

import twitter4j.User

data class User(
    @JvmField val twitterUserData: TwitterUser? = null,
)

data class TwitterUser(
    @JvmField val userId: String? = null,

    @JvmField val userName: String? = null
)

fun User?.toTwitterDisplayData(): TwitterUser {
    return if (this == null) {
        TwitterUser(
            userId = "not found", userName = "Twitterの名前が取得できませんでした"
        )
    } else {
        TwitterUser(
            userId = this.screenName, userName = this.name
        )
    }
}